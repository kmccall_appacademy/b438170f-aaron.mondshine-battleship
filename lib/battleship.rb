class BattleshipGame
  attr_accessor :player, :board

  def initialize(player = HumanPlayer.new, board = Board.random)
    @player = player
    @board = board
  end

  def attack(pos)
    board.grid[pos[0]][pos[1]] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = @player.get_play
    attack(pos)
  end
end
