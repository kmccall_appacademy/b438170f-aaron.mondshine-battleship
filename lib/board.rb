class Board
  attr_accessor :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    default = []
    rows = []
    10.times { rows << nil }
    10.times { default << rows }
    default
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def count
    ship_count = 0
    grid.each do |r|
      r.each do |c|
        ship_count += 1 if c == :s
      end
    end
    ship_count
  end

  def empty?(pos = nil)
    if pos == nil
      count == 0 ? true : false
    else
      row = pos[0]
      col = pos[1]
      grid[row][col] == nil ? true : false
    end
  end

  def full?
    count == grid.length**2 ? true : false
  end

  def place_random_ship
    raise 'error' if full?

    available_places = []
    grid.each_with_index do |r, idx1|
      r.each_index do |idx2|
        available_places << [idx1, idx2] if empty?([idx1, idx2])
      end
    end
    random_position = available_places.shuffle[0]
    @grid[random_position[0]][random_position[1]] = :s
  end

  def won?
    count == 0 ? true : false
  end

end
