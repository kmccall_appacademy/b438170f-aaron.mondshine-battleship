class HumanPlayer
  attr_reader :player

  def initialize(player)
    @player = player
  end

  def get_play
    puts "Where would you like to shoot?"
    gets.chomp
  end
end
